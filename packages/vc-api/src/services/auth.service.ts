import {Injectable, Logger} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {ApiCredential, IssuerDetails} from "../models/entity.js";
import {Repository} from "typeorm";

@Injectable()
export class AuthService {
    private authData = new Map<String, ApiCredential>();
    private apiKeyForDomain = new Map<String, String>();
    private logger = new Logger();

    constructor(
        @InjectRepository(ApiCredential) private readonly apiCredRepo: Repository<ApiCredential>
    ) {
        this.loadData().then(v => {
            this.logger.log("API keys loaded");
        })
    }

    private async loadData() {
        (await this.apiCredRepo.find({relations: ['issuer']})).forEach(v => {
            this.authData.set(v.apiKey, v);
            this.apiKeyForDomain.set(v.issuer.domain, v.apiKey);
        });
    }

    public isValidApiKey(apiKey: String): Boolean {
        return this.authData.has(apiKey);
    }

    public getIssuerDetailsByApiKey(apiKey: String): IssuerDetails {
        return this.authData.get(apiKey).issuer;
    }

    public getIssuerDetailsByDomain(domain: String): IssuerDetails {
        return this.authData.get(this.apiKeyForDomain.get(domain)).issuer;
    }
}