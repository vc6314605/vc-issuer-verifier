import {Injectable} from '@nestjs/common';
import {default as jsonld} from '@digitalcredentials/jsonld';

import {documentLoader} from "../utility/loader.js";
import {contextDomain, vcTypes} from "../utility/constants.js";
import {AsyncLocalStorage} from "./als.service.js";

@Injectable()
export class JsonLdValidator {

        constructor(
        private readonly asyncLocalStorage: AsyncLocalStorage
    ) {
    }

    public async validateCredentialOffer(credentialType, credentialSubject) {
        // TODO: [C]
            let input = {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                `${contextDomain}${vcTypes.get(credentialType).context}`,
                "https://w3id.org/security/suites/ed25519-2020/v1"
            ],
            "type": [
                "VerifiableCredential",
                credentialType
            ],
            "name": credentialType,
            "description": credentialType,
            "credentialSubject": {
                "id": "did:example",
                "type": vcTypes.get(credentialType).credentialSubjectType,
                ...credentialSubject
            }
        }
        // https://www.npmjs.com/package/jsonld

        let result = await jsonld.canonize(input, {
            "algorithm": "URDNA2015",
            "format": "application/n-quads",
            documentLoader: documentLoader,
            skipExpansion: false,
            expansionMap: info => {
                if (info.unmappedProperty) {
                    throw new Error('The property "' +
                        info.unmappedProperty + '" in the input ' +
                        'was not defined in the context.');
                }
            }
        });

        console.log(JSON.stringify(result));
    }
}
