import {SigningAlgorithm, VCPayload, VCSigner} from "./signers.js";
import * as jose from "jose";
import {JWK, JWTPayload, KeyLike} from "jose";
import {v4 as uuidv4} from "uuid";
import {DidDocument, IssuerKeyPair, PrivateKeyJwk} from "../../models/entity.js";
import {Errors} from "../../utility/Errors.js";

export class JwtSigner implements VCSigner {

    async sign(
        alg: SigningAlgorithm,
        payload: VCPayload,
        didDoc: DidDocument,
        keyPair: IssuerKeyPair,
        subjectDid: string
    ): Promise<any> {
        if (keyPair.type !== 'JsonWebKey2020') {
            throw Errors.INVALID_KEY_TYPE.exception("Only JsonWebKey2020 type supported at the moment")
        }
        const header = {
            alg: alg,
            kid: didDoc.id
        }
        let pvtKey = await jose.importJWK(keyPair.privateKeyJwk as JWK, alg);
        const vcJwt = await new jose.SignJWT(payload as unknown as JWTPayload)
            .setProtectedHeader(header)
            .setIssuer(didDoc.id)
            .setSubject(subjectDid)
            .setExpirationTime("72h")
            .setJti(`urn:uuid:${uuidv4()}`)
            .sign(pvtKey);

        return vcJwt;
    }
}