import {SigningAlgorithm, VCPayload, VCSigner} from "./signers.js";

import {Ed25519VerificationKey2020} from "@digitalcredentials/ed25519-verification-key-2020";
import {Ed25519Signature2020} from "@digitalcredentials/ed25519-signature-2020";
import vc from "@digitalcredentials/vc";
import {documentLoader} from "../../utility/loader.js";
import * as didKeyDriver from '@digitalcredentials/did-method-key'
import {DidDocument, IssuerKeyPair, PrivateKeyJwk} from "../../models/entity.js";
import {JsonWebKey2020} from "@transmute/json-web-signature";
import {SerializedKeyPair} from "@digitalcredentials/keypair";

export class LdpEd25519Signature2020Signer implements VCSigner {

    async sign(
        alg: SigningAlgorithm,
        payload: VCPayload,
        didDoc: DidDocument,
        keyPair: IssuerKeyPair,
        subjectDid: string
    ): Promise<any> {
        const keys = await Ed25519VerificationKey2020.from(keyPair);

        const suite = new Ed25519Signature2020({key: keys});

        suite.verificationMethod = didDoc.verificationMethod[0].id;

        const signedCredential = await vc.issue({
            credential: payload["vc"],
            suite,
            documentLoader
        });

        return signedCredential;
    }
}