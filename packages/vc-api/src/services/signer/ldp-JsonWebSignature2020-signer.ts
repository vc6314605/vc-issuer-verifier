import {VerifiableCredential} from "@transmute/vc.js/dist/types/VerifiableCredential.js";
import {verifiable} from '@transmute/vc.js';
import {SigningAlgorithm, VCPayload, VCSigner} from "./signers.js";

import {documentLoader} from "../../utility/loader.js";
// @ts-ignore
import {JsonWebKey, JsonWebKey2020, JsonWebSignature} from "@transmute/json-web-signature";
import {DidDocument, IssuerKeyPair, PrivateKeyJwk} from "../../models/entity.js";


export class LdpJsonWebSignature2020Signer implements VCSigner {
    async sign(
        alg: SigningAlgorithm,
        payload: VCPayload,
        didDoc: DidDocument,
        keyPair: IssuerKeyPair,
        subjectDid: string
    ): Promise<any> {
        payload.vc["@context"].push("https://w3id.org/security/suites/jws-2020/v1");

        const key = await JsonWebKey.from(keyPair as JsonWebKey2020);
        const suite = new JsonWebSignature({
                key,
            });
        const signedVC = await verifiable.credential.create({
            credential: payload.vc as VerifiableCredential,
            format: ['vc'],
            documentLoader: documentLoader,
            suite,
        });
        return signedVC.items[0];
    }

}