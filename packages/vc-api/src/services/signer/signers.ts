import {JwtSigner} from "./jwt-signer.js";
import {contextDomain, vcTypes} from "../../utility/constants.js";
import {LdpEd25519Signature2020Signer} from "./ldp-Ed25519Signature2020-signer.js";
import {LdpJsonWebSignature2020Signer} from "./ldp-JsonWebSignature2020-signer.js";
import {DidDocument, PrivateKeyJwk, IssuerKeyPair} from "../../models/entity.js";

export interface VCSigner{
    sign(
        alg: SigningAlgorithm,
        payload: VCPayload,
        didDoc: DidDocument,
        keyPair: IssuerKeyPair,
        subjectDid: string
    ): Promise<any>
}

export enum VCFormat{
    JWT = "JWT",
    LDP = "LDP",
}

export enum CryptographicSuite{
    // ss - Signature Suite - https://w3c-ccg.github.io/ld-cryptosuite-registry/#jsonwebsignature2020
    // alg - Algorithm - https://www.iana.org/assignments/jose/jose.xhtml#web-signature-encryption-algorithms
    SS_JsonWebSignature2020="JsonWebSignature2020",
    SS_Ed25519Signature2020="Ed25519Signature2020",
    ALG_ES256="ES256",
    ALG_ES256K="ES256K",
}

export enum SigningAlgorithm{
    ES256="ES256",
    ES256K="ES256K"
}


export interface VCPayloadVC{
    "@context": string[],
    type: string[],
    name: string,
    description: string,
    credentialSubject: any,
    id: string,
    issuer: string,
    issuanceDate: string,
}

export interface VCPayload{
    vc: VCPayloadVC
}


export class VCSignerFactory{
    static get(
        vcFormat: VCFormat,
        cryptographicSuite: CryptographicSuite,
        signingAlgorithm: SigningAlgorithm
    ) : VCSigner{
        switch (vcFormat){
            case VCFormat.JWT:
                return new JwtSigner();
            case VCFormat.LDP:
                switch (cryptographicSuite) {
                    case CryptographicSuite.SS_Ed25519Signature2020:
                        return new LdpEd25519Signature2020Signer();
                    case CryptographicSuite.SS_JsonWebSignature2020:
                        return new LdpJsonWebSignature2020Signer();
                }
        }
        throw new Error(`Unsupported signer requirement | vcFormat: ${vcFormat} | cryptographicSuite: ${cryptographicSuite} | signingAlgorithm: ${signingAlgorithm}`);
    }
}