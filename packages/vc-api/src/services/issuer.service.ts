import {Injectable, Logger} from '@nestjs/common';
import {
    CredentialIssueRequest,
    CredentialIssueResponse,
    CredentialOfferResponse,
    GenerateCredentialOfferRequest,
    GenerateCredentialOfferResponse,
    GenerateTokenRequest,
    GenerateTokenResponse,
    IssuerStatus
} from "../models/dto.js";
import {PreAuthService} from "./pre-auth.service.js";
import {v4 as uuidv4} from "uuid";
import * as jose from "jose";
import {SecretManager} from "./secret-manager.service.js";
import {DateTime} from "luxon";
import {contextDomain, vcTypes} from "../utility/constants.js";
import {AsyncLocalStorage} from "./als.service.js";
import {DataStore, DataStoreMap} from "./data-store.service.js";
import {CryptographicSuite, SigningAlgorithm, VCFormat, VCSigner, VCSignerFactory} from "./signer/signers.js";
import {Errors} from "../utility/Errors.js";
import {JsonLdValidator} from "./json-ld-validator.js";

@Injectable()
export class IssuerService {
    logger = new Logger("IssuerService");


    //preAuthCode, VC body
    private issuanceRequests: DataStoreMap<GenerateCredentialOfferRequest>;
    private responseMapping: DataStoreMap<string>; //preAuthCode/token, responseUUID
    private issuanceResponses: DataStoreMap<IssuerStatus>; //responseUUID, status
    private issuanceTokenRequests: DataStoreMap<string>; // token, preAuthCode

    constructor(
        private readonly preAuthService: PreAuthService,
        private readonly secretManager: SecretManager,
        private readonly asyncLocalStorage: AsyncLocalStorage,
        private readonly dataStore: DataStore,
        private readonly jsonLdValidator: JsonLdValidator
    ) {
        const commonTTL = 1*60*60*1000;
        this.issuanceRequests = this.dataStore.getStore<GenerateCredentialOfferRequest>("issuanceRequests", commonTTL);
        this.responseMapping = this.dataStore.getStore<string>("responseMapping", commonTTL);
        this.issuanceResponses = this.dataStore.getStore<IssuerStatus>("issuanceResponses", commonTTL);
        this.issuanceTokenRequests = this.dataStore.getStore<string>("issuanceTokenRequests", commonTTL);
    }

    async createCredentialOffer(credentialOfferRequest: GenerateCredentialOfferRequest): Promise<GenerateCredentialOfferResponse> {
        const issuerDetails = this.asyncLocalStorage.getCurrentIssuerDetails();

        // TODO: [C] take from meta data
        if(!issuerDetails.credType.includes(credentialOfferRequest.type)){
            throw Errors.UNAUTHORIZED_CREDENTIAL_TYPE.exception();
        }

        await this.jsonLdValidator.validateCredentialOffer(credentialOfferRequest.type, credentialOfferRequest.credentialSubject);

        const preAuthcode: string = await this.preAuthService.newPreAuthCode(credentialOfferRequest.type);
        await this.issuanceRequests.set(preAuthcode, credentialOfferRequest);

        const response: CredentialOfferResponse = {
            issuer: issuerDetails.getIssuerUrl(),
            credential_type: credentialOfferRequest.type,
            "pre-authorized_code": preAuthcode,
            user_pin_required: false
        }

        const params = Object.keys(response).map(key => {
            return `${key}=${encodeURIComponent(response[key])}`;
        }).join("&")

        const vcIssuanceUrl: string = "openid-initiate-issuance://?" + params;
        
        // TODO: [C] what are the issues vcs, requests save in DB 
        
        const responseUUID = uuidv4();
        await this.responseMapping.set(preAuthcode, responseUUID); // TODO: [C] can keep as a claim 
        await this.issuanceResponses.set(responseUUID, {
            status: "In Progress"
        });
        this.logger.log(`State saved: "${responseUUID}" - ${JSON.stringify(await this.issuanceResponses.get(responseUUID))}`)
        const vcIssuanceStatusUrl = `${this.asyncLocalStorage.getRequestHostUrl()}/api/credential-offer/${responseUUID}/status`;

        return {
            vcIssuanceUrl: vcIssuanceUrl,
            vcIssuanceStatusUrl: vcIssuanceStatusUrl
        }
    }

    async generateAccessToken(tokenRequest: GenerateTokenRequest): Promise<GenerateTokenResponse> {
        const accessToken = await this.preAuthService.newAccessToken(tokenRequest["pre-authorized_code"]);
        await this.issuanceTokenRequests.set(accessToken, tokenRequest["pre-authorized_code"]);
        await this.responseMapping.set(accessToken, await this.responseMapping.get(tokenRequest["pre-authorized_code"]));

        const token = {
            access_token: accessToken,
            token_type: "bearer",
            expires_in: 3600 // TODO: [C] change later, use same in newAccessToken and here
        }

        return token;
    }

    async getCredentialOfferStatus(state: string): Promise<IssuerStatus> {
        let status = await this.issuanceResponses.get(state);
        if (!status) {
            this.logger.log(`Issuance ID not found: "${state}"`)
            throw Errors.INCORRECT_ISSUANCE_ID.exception();
        }
        return status;
    }

    async issueVC(credentialIssueRequest: CredentialIssueRequest, accessToken: string): Promise<CredentialIssueResponse> {
        // TODO: [C] we can take the response UUID from the accessToken as well, then no need to maintain responseMapping map
        if(!(await this.preAuthService.validateAccessToken(accessToken))){
            // TODO: [C] can use https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-credential-error-response
            throw Errors.INVALID_ACCESS_TOKEN.exception();
        }

        const issuerApiUrl = this.asyncLocalStorage.getRequestHostUrl();
        const jwt = credentialIssueRequest.proof.jwt;
        const protectedHeader = jose.decodeProtectedHeader(jwt);

        const subjectDid = protectedHeader.kid.split("#")[0]; //remove the #0 at the end of the did:jwk

        //generate the VC

        let issuerDetails = this.asyncLocalStorage.getCurrentIssuerDetails();

        const id = `urn:uuid:${uuidv4()}`;

        let credentialOfferReq = await this.issuanceRequests.get(await this.issuanceTokenRequests.get(accessToken));
        if (!credentialOfferReq)
            throw Errors.INVALID_ACCESS_TOKEN.exception(); // TODO: [C] make spec compliant

        const payload = {
            vc: {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1",
                    `${contextDomain || issuerApiUrl}${vcTypes.get(credentialOfferReq.type).context}`
                ],
                type: [
                    "VerifiableCredential",
                    credentialOfferReq.type
                ],
                name: credentialOfferReq.name || credentialOfferReq.type,
                description: credentialOfferReq.description || credentialOfferReq.type,
                credentialSubject: {
                    id: subjectDid,
                    type: vcTypes.get(credentialOfferReq.type).credentialSubjectType,
                    ...credentialOfferReq.credentialSubject
                },
                id,
                issuer: issuerDetails.didDocument.id,
                issuanceDate: DateTime.now().setZone('UTC').toISO()
            }
        }

        const privateKey = await this.secretManager.getPrivateKey();

        let signer: VCSigner = null;

        if (credentialIssueRequest.format === "jwt_vc") {
            signer = VCSignerFactory.get(
                VCFormat.JWT,
                issuerDetails.alg as CryptographicSuite,
                issuerDetails.alg as SigningAlgorithm
            );
        } else if (credentialIssueRequest.format === "ldp_vc") {
             signer = VCSignerFactory.get(
                VCFormat.LDP,
                issuerDetails.metaData.credentials_supported[credentialOfferReq.type].formats.ldp_vc.cryptographic_suites_supported[0] as CryptographicSuite,
                issuerDetails.alg as SigningAlgorithm
            );
        }
        if(!signer){
            // TODO: [C] follow the spec
            throw Errors.INVALID_CREDENTIAL_FORMAT.exception();
        }

        const credential = await signer.sign(
                    issuerDetails.alg as SigningAlgorithm,
                    payload,
                    issuerDetails.didDocument,
                    issuerDetails.keyPair,
                    subjectDid
                );

        this.logger.log(`Payload: ${JSON.stringify(payload)}`);

        await this.issuanceResponses.set(await this.responseMapping.get(accessToken), {
            status: "Issued"
        });

        return { format: credentialIssueRequest.format, credential };
    }
}
