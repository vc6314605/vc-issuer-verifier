import {
    EntityManager,
    EntityTarget,
    FindManyOptions,
    FindOneOptions,
    FindOptionsWhere,
    ObjectId, QueryRunner,
    Repository
} from "typeorm";
import {ApiCredential, IssuerDetails} from "../../models/entity.js";
import {Injectable} from "@nestjs/common";
import * as process from "process";

@Injectable()
export class DummyApiCredRepo extends Repository<ApiCredential> {
    private data: ApiCredential[] = JSON.parse(`
[
        {
            "credentialId": 2,
            "apiKey": "b3df7181-b54f-49ad-9b3b-3695b53b5960",
            "issuer": {
                "issuerId": 4,
                "domain": "${process.env.ISSUER_DOMAIN_NAME}",
                "alg": "ES256",
                "metaData": {
                    "issuer": "https://${process.env.ISSUER_DOMAIN_NAME}",
                    "jwks_uri": "https://${process.env.ISSUER_DOMAIN_NAME}/jwks",
                    "token_endpoint": "https://${process.env.ISSUER_DOMAIN_NAME}/token",
                    "credential_endpoint": "https://${process.env.ISSUER_DOMAIN_NAME}/credential",
                    "credentials_supported": {
                        "EmployeeCredential": {
                            "formats": {
                                "ldp_vc": {
                                    "types": [
                                        "VerifiableCredential", "EmployeeCredential"
                                    ],
                                    "cryptographic_suites_supported": ["Ed25519Signature2020"],
                                    "cryptographic_binding_methods_supported": ["did"]
                                }
                            }
                        }
                    },
                    "grant_types_supported": ["urn:ietf:params:oauth:grant-type:pre-authorized_code"]
                },
                "credType": "EmployeeCredential",
                "keyPair": {
                    "id": "did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD",
                    "type": "Ed25519VerificationKey2020",
                    "controller": "did:web:${process.env.ISSUER_DOMAIN_NAME}",
                      "publicKeyMultibase": "z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD",
                      "privateKeyMultibase": "zrv2foQsS1b84k8LAjvJX5xh8RnspnJQuSfqFAWhoc8DYDq7ZkiK5XpX8pEVNMPqvj9eTeW1EBgGwvgRg3wZ6REC1Q5"
                },
                "didDocument": {
                    "id": "did:web:${process.env.ISSUER_DOMAIN_NAME}",
                    "@context": [
                        "https://www.w3.org/ns/did/v1", "https://w3id.org/security/suites/jws-2020/v1"
                    ],
                    "authentication": ["did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"],
                    "assertionMethod": ["did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"],
                    "verificationMethod": [
                        {
                            "id": "did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD",
                            "type": "Ed25519VerificationKey2020",
                            "controller": "did:web:${process.env.ISSUER_DOMAIN_NAME}",
                            "publicKeyMultibase": "z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"
                        }
                    ]
                },
                "tokenJwk": {
                    "d": "FGqgExAReVFA2_2JdmbRsOmgQH5BX_E9wDCZE5hY2Gw",
                    "x": "q8xhH-VaFkUgvux1ELcoI3JSXG458Mko7mnEl3hh7PE",
                    "y": "CU8p9j_3bG5paTrzBKFkZoLtKAze5g0ddH37_oVF9so",
                    "crv": "P-256",
                    "kty": "EC"
                }
            }
        }, {
            "credentialId": 1,
            "apiKey": "38e29b48-0f38-4490-909a-cfb41a7f3f0c",
            "issuer": {
                "issuerId": 1,
                "domain": "${process.env.ISSUER_DOMAIN_NAME}",
                "alg": "ES256",
                "metaData": {
                    "issuer": "https://${process.env.ISSUER_DOMAIN_NAME}",
                    "jwks_uri": "https://${process.env.ISSUER_DOMAIN_NAME}/jwks",
                    "token_endpoint": "https://${process.env.ISSUER_DOMAIN_NAME}/token",
                    "credential_endpoint": "https://${process.env.ISSUER_DOMAIN_NAME}/credential",
                    "credentials_supported": {
                        "OrderCredential": {
                            "formats": {
                                "ldp_vc": {
                                    "types": [
                                        "VerifiableCredential", "OrderCredential"
                                    ],
                                    "cryptographic_suites_supported": ["Ed25519Signature2020"],
                                    "cryptographic_binding_methods_supported": ["did"]
                                }
                            }
                        },
                        "EmployeeCredential": {
                            "formats": {
                                "ldp_vc": {
                                    "types": [
                                        "VerifiableCredential", "EmployeeCredential"
                                    ],
                                    "cryptographic_suites_supported": ["Ed25519Signature2020"],
                                    "cryptographic_binding_methods_supported": ["did"]
                                }
                            }
                        },
                        "LoyaltyTierCredential": {
                            "formats": {
                                "ldp_vc": {
                                    "types": [
                                        "VerifiableCredential", "LoyaltyTierCredential"
                                    ],
                                    "cryptographic_suites_supported": ["Ed25519Signature2020"],
                                    "cryptographic_binding_methods_supported": ["did"]
                                }
                            }
                        },
                        "LoyaltyMembershipCredential": {
                            "formats": {
                                "ldp_vc": {
                                    "types": [
                                        "VerifiableCredential", "LoyaltyMembershipCredential"
                                    ],
                                    "cryptographic_suites_supported": ["Ed25519Signature2020"],
                                    "cryptographic_binding_methods_supported": ["did"]
                                }
                            }
                        }
                    },
                    "grant_types_supported": ["urn:ietf:params:oauth:grant-type:pre-authorized_code"]
                },
                "credType": "LoyaltyMembershipCredential,OrderCredential,EmployeeCredential,LoyaltyTierCredential",
                "keyPair": {
                    "id": "did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD",
                    "type": "Ed25519VerificationKey2020",
                    "controller": "did:web:${process.env.ISSUER_DOMAIN_NAME}",
                     "publicKeyMultibase": "z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD",
                     "privateKeyMultibase": "zrv2foQsS1b84k8LAjvJX5xh8RnspnJQuSfqFAWhoc8DYDq7ZkiK5XpX8pEVNMPqvj9eTeW1EBgGwvgRg3wZ6REC1Q5"
                },
                "didDocument": {
                    "id": "did:web:${process.env.ISSUER_DOMAIN_NAME}",
                    "@context": [
                        "https://www.w3.org/ns/did/v1", "https://w3id.org/security/suites/jws-2020/v1"
                    ],
                    "authentication": ["did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"],
                    "assertionMethod": ["did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"],
                    "verificationMethod": [
                        {
                            "id": "did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD",
                            "type": "Ed25519VerificationKey2020",
                            "controller": "did:web:${process.env.ISSUER_DOMAIN_NAME}",
                            "publicKeyMultibase": "z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"
                        }
                    ]
                },
                "tokenJwk": {
                    "d": "FGqgExAReVFA2_2JdmbRsOmgQH5BX_E9wDCZE5hY2Gw",
                    "x": "q8xhH-VaFkUgvux1ELcoI3JSXG458Mko7mnEl3hh7PE",
                    "y": "CU8p9j_3bG5paTrzBKFkZoLtKAze5g0ddH37_oVF9so",
                    "crv": "P-256",
                    "kty": "EC"
                }
            }
        }
    ]    
    `);


    constructor() {
        super(null, null, null);
        this.data = this.data.map(v=>Object.assign(new ApiCredential(), v));
        this.data.forEach(apiCred=>{
            apiCred.issuer = Object.assign(new IssuerDetails(), apiCred.issuer)
        })
    }

    find(options?: FindManyOptions<ApiCredential>): Promise<ApiCredential[]> {
        return Promise.resolve(this.data);
    }
}