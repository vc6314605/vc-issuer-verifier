import {Injectable} from '@nestjs/common';
import {PreAuthService} from "./pre-auth.service.js";
import {SecretManager} from "./secret-manager.service.js";
import {AsyncLocalStorage} from "./als.service.js";
import {DataStore} from "./data-store.service.js";

@Injectable()
export class MetaDataService {

    constructor(
        private readonly preAuthService: PreAuthService,
        private readonly secretManager: SecretManager,
        private readonly asyncLocalStorage: AsyncLocalStorage,
        private readonly dataStore: DataStore
    ) {

    }

    public getJwks() {
        // TODO: [C] can we multibase as a JWK?
        return {
            keys: [this.asyncLocalStorage.getCurrentIssuerDetails().keyPair.publicKeyJwk]
        }
    }
    
    public getMetadata() {
        let issuerData = this.asyncLocalStorage.getCurrentIssuerDetails();
        let isserUrl = this.asyncLocalStorage.getCurrentIssuerDetails().getIssuerUrl();
        const metaData = {
            issuer: `${isserUrl}`,
            credential_endpoint: `${isserUrl}/credential`,
            token_endpoint: `${isserUrl}/token`,
            jwks_uri: `${isserUrl}/jwks`,
            ...issuerData.metaData
        }

        return metaData;
    }
}