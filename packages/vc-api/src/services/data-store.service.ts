import {Inject, Injectable} from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';

export class DataStoreMap<V>{

    constructor(
        private keyPrefix: string,
        private ttl: number,
        private cacheManager: Cache
    ){
    }

    async delete(key: string): Promise<void> {
        const finalKey = `${this.keyPrefix}${key}`;
        return await this.cacheManager.del(finalKey);
    }

    async get(key: string): Promise<V | undefined> {
        const finalKey = `${this.keyPrefix}${key}`;
        let cachedValue = await this.cacheManager.get(finalKey);
        if(cachedValue){
            // @ts-ignore
            return JSON.parse(cachedValue);
        }
        return undefined;
    }

    async has(key: string): Promise<boolean> {
        const finalKey = `${this.keyPrefix}${key}`;
        return !!(await this.cacheManager.get(finalKey));
    }

    async set(key: string, value: V): Promise<void> {
        const finalKey = `${this.keyPrefix}${key}`;
        if(value){
            return await this.cacheManager.set(finalKey, JSON.stringify(value), this.ttl);
        }
    }
}

@Injectable()
export class DataStore {

    constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

    getStore<V>(name: string, ttl: number):DataStoreMap<V>{
        return new DataStoreMap<V>(name, ttl, this.cacheManager);
    }
}


