import {Injectable, Logger} from "@nestjs/common";
import {SecretManager} from "./secret-manager.service.js";
import * as jose from "jose";
import {AsyncLocalStorage} from "./als.service.js";
import {Errors} from "../utility/Errors.js";

const TOKEN_TYPE_PRE_AUTH = "PRE_AUTH";
const TOKEN_TYPE_ACCESS_TOKEN = "ACCESS_TOKEN";

@Injectable()
export class PreAuthService {
    private logger = new Logger("PreAuthService");

    private preAuthCodes = new Map<string, string>();


    constructor(
        private readonly secretManager: SecretManager,
        private readonly asyncLocalStorage: AsyncLocalStorage,
    ) {
    }

    public async newPreAuthCode(credType: string): Promise<string> {
        let issuerDetails = this.asyncLocalStorage.getCurrentIssuerDetails();

        const payload = {
            credential_type: credType,
            token_type: TOKEN_TYPE_PRE_AUTH,
        };

        const header = {
            alg: issuerDetails.alg
        };

        const privateKey = await this.secretManager.getPrivateKey();
        const preAuthcode = await new jose.SignJWT(payload)
            .setProtectedHeader(header)
            .setIssuer(issuerDetails.getIssuerUrl())
            .setExpirationTime("6h") // TODO: [C] change later
            .sign(privateKey);

        this.preAuthCodes.set(preAuthcode, preAuthcode);

        return preAuthcode;
    }


    public async validatePreAuthCode(preAuthCode: string): Promise<AccessTokenData | false> {
        let issuerDetails = this.asyncLocalStorage.getCurrentIssuerDetails();

        try {
            let verifyResult = await jose.jwtVerify(preAuthCode, await this.secretManager.getPublicKey());
            if (
                issuerDetails.credType.includes(verifyResult['payload']['credential_type'] as string)  &&
                verifyResult['payload']['iss'] === issuerDetails.getIssuerUrl() &&
                verifyResult['payload']['token_type'] === TOKEN_TYPE_PRE_AUTH
            ) {
                return {
                    issuerUrl: verifyResult['payload']['iss'],
                    credentialType: verifyResult['payload']['credential_type'] as string
                };
            } else {
                this.logger.error("Pre-auth code validate error", verifyResult);
                return false;
            }
        } catch (e) {
            this.logger.error("Pre-auth code validate error", e);
            return false;
        }
    }

    public async validateAccessToken(accessToken: string): Promise<AccessTokenData | false> {
        let issuerDetails = this.asyncLocalStorage.getCurrentIssuerDetails();

        try {
            let verifyResult = await jose.jwtVerify(accessToken, await this.secretManager.getPublicKey());
            if (
                issuerDetails.credType.includes(verifyResult['payload']['credential_type'] as string) &&
                verifyResult['payload']['iss'] === issuerDetails.getIssuerUrl() &&
                verifyResult['payload']['token_type'] === TOKEN_TYPE_ACCESS_TOKEN
            ) {
                return {
                    issuerUrl: verifyResult['payload']['iss'],
                    credentialType: verifyResult['payload']['credential_type'] as string
                };
            } else {
                this.logger.error("Access token validate error", verifyResult);
                return false;
            }
        } catch (e) {
            this.logger.error("Access token code validate error", e);
            return false;
        }
    }

    public async newAccessToken(preAuthCode: string) {
        const issuerDetails = this.asyncLocalStorage.getCurrentIssuerDetails();
        let preAuthCodeData = await this.validatePreAuthCode(preAuthCode);
        if (preAuthCodeData) {
            const payload = {
                credential_type: preAuthCodeData.credentialType,
                token_type: TOKEN_TYPE_ACCESS_TOKEN,
            };

            const header = {
                alg: issuerDetails.alg
            };
            const privateKey = await this.secretManager.getPrivateKey();
            const accessToken = await new jose.SignJWT(payload)
                .setProtectedHeader(header)
                .setIssuer(issuerDetails.getIssuerUrl())
                .setExpirationTime("1h")
                .sign(privateKey);

            this.preAuthCodes.set(preAuthCode, accessToken);

            return accessToken;
        } else {
            throw Errors.INVALID_PRE_AUTH_CODE.exception();
        }
    }

}

export interface AccessTokenData {
    issuerUrl: string;
    credentialType: string;
}