import * as jose from "jose";
import {Ed25519VerificationKey2020} from "@digitalcredentials/ed25519-verification-key-2020";
import {Ed25519Signature2020} from '@digitalcredentials/ed25519-signature-2020';
import {Injectable} from "@nestjs/common";
import {AsyncLocalStorage} from "./als.service.js";
import {JWK} from "jose";

const keyPair: any = {
    "kty": "EC",
    "x": "qD5P9giP36vQwQ5DPvr7mewBjmo-q3nRsGnAN6H2niQ",
    "y": "eU02oJpUDWULyL42fKBwWhvLX6TYdUZ6jniu7GCvDXI",
    "crv": "secp256k1",
    "d": "cKTYQicFbZgDXb9QAuAJ1iyzMDSeVKSkD4dYbAM-ISY"
};

export const alg = "ES256K";

@Injectable()
export class SecretManager {

    constructor(private asyncLocalStorage: AsyncLocalStorage) {
    }

    public async getPublicKey() {
        return await jose.importJWK(this.asyncLocalStorage.getCurrentIssuerDetails().tokenJwk as JWK, this.asyncLocalStorage.getCurrentIssuerDetails().alg);
    }

    public async getPrivateKey() {
        return await jose.importJWK(this.asyncLocalStorage.getCurrentIssuerDetails().tokenJwk as JWK, this.asyncLocalStorage.getCurrentIssuerDetails().alg);
    }

    public async getPublicKeyAsDidJwk() {
        const publicKey = await this.getPublicKey();

        const exported = await jose.exportJWK(publicKey)
        const asObject = JSON.parse(JSON.stringify(exported));
        asObject.alg = "ES256K";
        asObject.use = "sig";
        var base64 = Buffer.from(JSON.stringify(asObject)).toString("base64url");

        const kid = `did:jwk:${base64}`;
        return kid;
    }

    ///////////////////////////////////////////////////////////////

    private key = {
        "id": "did:web:corpa.digitalid.provesio.com#z6MkjLpYN5w3B1qAUQHoPBn8Dd7pmUdMo7UgcftAXC3tqd8J",
        "type": "Ed25519VerificationKey2020",
        "controller": "did:web:corpa.digitalid.provesio.com",
        "publicKeyMultibase": "z6MkjLpYN5w3B1qAUQHoPBn8Dd7pmUdMo7UgcftAXC3tqd8J",
        "privateKeyMultibase": "zrv3pmwmwjE9KUK4F8FDPkTe2o6sp3UbzVYU5AXX56SAaE1UYTjbhRbykcwuM6Xa93AAhYF4gchL1g2KyVatax339Cz"
    };

    public async getSuite(issuer: string) {
        this.key.controller = await this.getPublicKeyAsDidJwk2(issuer);
        this.key.id = await this.getPublicKeyAsDidJwk2(issuer) + "#0";

        const issuerKeyPair = await Ed25519VerificationKey2020.from(this.key);
        const issuerSuite = new Ed25519Signature2020({
            key: issuerKeyPair
        });
        return issuerSuite;
    }

    public async getPublicKeyAsDidJwk2(issuer: string) {
        const issuerKeyPair = await Ed25519VerificationKey2020.from(this.key);
        const exported = issuerKeyPair.toJwk({publicKey: true, privateKey: false});

        const asObject = JSON.parse(JSON.stringify(exported));
        asObject.alg = "EdDSA";
        asObject.use = "sig";
        var base64 = Buffer.from(JSON.stringify(asObject)).toString("base64url");

        const kid = `did:jwk:${base64}`;
        return kid;
    }

}