import {Injectable} from '@nestjs/common';

import {ClsService} from 'nestjs-cls';
import {AuthService} from "./auth.service.js";
import {IssuerDetails} from "../models/entity.js";
import {Errors} from "../utility/Errors.js";

@Injectable()
export class AsyncLocalStorage {

    constructor(
        private readonly clsService: ClsService,
        private readonly authService: AuthService
    ) {
    }


    public getCurrentIssuerDetails(): IssuerDetails {
        let apiKey = this.clsService.get("currentUserApiKey");
        try {
            return apiKey ? this.authService.getIssuerDetailsByApiKey(this.clsService.get("currentUserApiKey")) : this.authService.getIssuerDetailsByDomain(this.getRequestDomain());
        } catch (e) {
            console.error(`Error while getting issuer details | domain: ${this.getRequestDomain()} | apiKey: ${apiKey ? apiKey.substring(0, Math.min(apiKey.length, 5)) : "N/A"}`, e);
            throw Errors.UNKNOWN_REQUEST.exception();
        }
    }

    public getRequestHostUrl(): string {
        return this.clsService.get("hostUrl");
    }

    public getRequestDomain(): string {
        return this.clsService.get("hostDomain");
    }

    public getRequestId(): string {
        return this.clsService.get("reqId");
    }
}
