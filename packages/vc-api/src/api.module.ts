import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {ApiController} from "./controllers/api.controller.js";
import {IssuerService} from "./services/issuer.service.js";
import {PreAuthService} from "./services/pre-auth.service.js";
import {SecretManager} from "./services/secret-manager.service.js";
import {PassportModule} from "@nestjs/passport";
import {ApiKeyStrategy} from "./auth/api-key.strategy.js";
import {WalletController} from "./controllers/wallet.controller.js";
import {TypeOrmModule} from "@nestjs/typeorm";
import {HealthController} from "./controllers/healthController.js";
import {ApiCredential, IssuerDetails} from "./models/entity.js";
import {AuthService} from "./services/auth.service.js";
import {ClsModule} from "nestjs-cls";
import {AsyncLocalStorage} from "./services/als.service.js";
import {CacheModule} from "@nestjs/cache-manager";
import {DataStore} from "./services/data-store.service.js";
import {MetaDataService} from "./services/meta.service.js";
import * as process from "process";
import {v4 as uuidv4} from "uuid";
import {ResponseHeadersMiddleware} from "./utility/middlewares.js";
import {JsonLdValidator} from "./services/json-ld-validator.js";
import {LoggerMiddleware} from "./utility/logger-middleware.js";
import {Type} from "@nestjs/common/interfaces/type.interface.js";
import {DummyApiCredRepo} from "./services/dummy/DummyApiCredRepo.js";
import {Provider} from "@nestjs/common/interfaces/modules/provider.interface.js";
import {DynamicModule} from "@nestjs/common/interfaces/modules/dynamic-module.interface.js";
import {ForwardReference} from "@nestjs/common/interfaces/modules/forward-reference.interface.js";
import {Abstract} from "@nestjs/common/interfaces/abstract.interface.js";
import { ServeStaticModule } from '@nestjs/serve-static';
import path from "path";

const myModuleImports = [
    ClsModule.register({
        middleware: {
            // automatically mount the
            // ClsMiddleware for all routes
            mount: true,
            // and use the setup method to
            // provide default store values.
            setup: (cls, req) => {
                cls.set('currentUserApiKey', req.headers['x-api-key']);

                const proto = req.headers['x-forwarded-proto'] || 'http';
                const host = req.headers['host'];
                cls.set('hostUrl', `${proto}://${host}`);
                cls.set('hostDomain', host);

                let reqId = req.headers['x-req-id'];
                if (!reqId) {
                    reqId = uuidv4();
                    req.headers['x-req-id'] = reqId;
                }
                cls.set('reqId', reqId);
            },
        },
    }),

    CacheModule.register(),
    PassportModule.register({
        defaultStrategy: 'apikey'
    })
];

const myProviders:Provider[] = [
        AuthService,
        ApiKeyStrategy,
        IssuerService,
        PreAuthService,
        SecretManager,
        AsyncLocalStorage,
        DataStore,
        JsonLdValidator,
        MetaDataService,
    ];

const myControllers:Type<any>[] = [
        ApiController,
        WalletController
    ];

const myExports:Array<DynamicModule | Promise<DynamicModule> | string | symbol | Provider | ForwardReference | Abstract<any> | Function> = [];

if (process.env.POSTGRES_DB_ENABLED == 'true') {
    myModuleImports.push(
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT),
            password: process.env.DB_PASS,
            username: process.env.DB_USER,
            entities: [ApiCredential, IssuerDetails],
            database: process.env.DB_NAME,
            synchronize: true,
            logging: true,
        }),
        TypeOrmModule.forFeature([ApiCredential, IssuerDetails])
    )
    myControllers.push(HealthController)
}else {
    myProviders.push({
        provide: 'ApiCredentialRepository',
        useClass: DummyApiCredRepo
    });
}

@Module({
    imports: myModuleImports,
    controllers: myControllers,
    providers: myProviders,
    exports: myExports
})
export class ApiModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(ResponseHeadersMiddleware)
            .forRoutes('*');
        consumer
            .apply(LoggerMiddleware)
            .forRoutes('*');
    }
}