export interface GenerateCredentialOfferRequest {
    type: string,
    name: string,
    description: string,
    credentialSubject: any,
}

export interface GenerateCredentialOfferResponse {
    vcIssuanceUrl: string,
    vcIssuanceStatusUrl: string,
}

export interface CredentialOfferResponse {
    issuer: string,
    credential_type: string,
    "pre-authorized_code": string,
    user_pin_required: boolean
}

export interface GenerateTokenRequest {
  credential_type: string,
  'pre-authorized_code': string,
  user_pin_required: boolean,
}

export interface GenerateTokenResponse{
    access_token: string,
    token_type: string,
    expires_in: number
}

export interface IssuerStatus{
    status: string
}

export interface CredentialIssueRequest {
    type: string,
    format: string,
    proof: {
        proof_type: string,
        jwt: string
    }
}

export interface CredentialIssueResponse{
    format: string,
    credential: any
}


export interface AuthorizationRequest {
    type: string,
    authorizationRequirements: AuthorizationRequirements
}

export interface AuthorizationRequirements {
    purpose: string,
    description: string,
    minCredentials: number,
    maxCredentials: number
}

export interface AuthorizationResponse{
    vpAuthorizationUrl: string,
    vpAuthorizationStatusUrl: string
}

export interface VerificationStatus {
    request: AuthorizationRequest,
    response: VerificationResponse
}

export interface VerificationResponse {
    status: string,
    credentialSubject?: any[]
}

export interface VerificationResponse {
    status: string,
    credentialSubject?: any[]
}

export interface PresentationVerificationRequest {
    expires_in: string,
    state: string,
    vp_token: string,
    id_token: string
}