import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";

export interface PublicKeyJwk {
    kty: string;
    crv?: string;
    x?: string;
    y?: string;
}
export interface PrivateKeyJwk {
    kty: string;
    crv?: string;
    x?: string;
    y?: string;
    d?: string;
}

export interface IssuerKeyPair {
    '@context'?: string;
    id?: string;
    type?: string;
    controller?: string;
    revoked?: string;
    /**
     * Public / private key material
     */
    publicKeyBase58?: string;
    privateKeyBase58?: string;
    publicKeyMultibase?: string;
    privateKeyMultibase?: string;
    publicKeyJwk?: PublicKeyJwk;
    privateKeyJwk?: PrivateKeyJwk;
}

export interface DidDocument {
    "@context": string[];
    id: string;
    verificationMethod: VerificationMethod[];
    authentication: string[];
    assertionMethod: string[];
}

export interface VerificationMethod {
    id: string;
    type: string;
    controller: string;
    publicKeyMultibase?: string;
    publicKeyJwk?: any;
}

export interface SupportCredentialFormatInfo{
    types: string[];
    cryptographic_suites_supported: string[];
    cryptographic_binding_methods_supported: string[];
}
export interface SupportCredentialFormat{
    ldp_vc?: SupportCredentialFormatInfo
    jwt_vc?: SupportCredentialFormatInfo
}
export interface SupportCredentialInfo{
    formats: SupportCredentialFormat
}
export interface IssuerMetaData{
    issuer: string;
    jwks_uri: string;
    token_endpoint: string;
    credential_endpoint: string;
    credentials_supported: Map<string,SupportCredentialInfo>;
    grant_types_supported: string[];
}

@Entity("issuer")
export class IssuerDetails {
    @PrimaryColumn({name: "issuer_id", type: 'integer'})
    issuerId: number;

    @Column({name: "domain", type: 'varchar', length: 200, nullable: true})
    domain: string;

    @Column({name: "alg", type: 'varchar', length: 20, nullable: true})
    alg: string;

    @Column({name: "meta_data", type: 'jsonb', nullable: true})
    metaData: IssuerMetaData;

    @Column({name: "cred_type", type: 'varchar', length: 100, nullable: true})
    credType: string;

    @Column({name: "key_pair", type: 'jsonb', nullable: true})
    keyPair: IssuerKeyPair;

    @Column({name: "did", type: 'jsonb', nullable: true})
    didDocument: DidDocument;

    @Column({name: "token_jwk", type: 'jsonb', nullable: true})
    tokenJwk: PrivateKeyJwk;

    @OneToMany(() => ApiCredential, apiCredential => apiCredential.issuer)
    apiCredentials: ApiCredential[];

    public getIssuerUrl(){
        return `https://${this.domain}`;
    }
}

@Entity("api_credential")
export class ApiCredential {
    @PrimaryColumn({name: "credential_id", type: 'integer'})
    credentialId: number;

    @Column({name: "api_key", type: 'varchar', length: 100})
    apiKey: string;

    @ManyToOne(() => IssuerDetails, (issuerDetails)=>issuerDetails.issuerId)
    @JoinColumn({ name: "issuer_id" })
    issuer: IssuerDetails;
}

