import {createParamDecorator, ExecutionContext} from '@nestjs/common';
import {Request} from 'express';

export const HostUrl = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest<Request>();
        const proto = request.get('x-forwarded-proto') || 'http';
        const host = request.get('host');
        return `${proto}://${host}`;
    },
);

export const BearerAuthToken = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest<Request>();
        return request.get('authorization').substring(7, request.get('authorization').length);
    },
);
