import {Injectable, NestMiddleware} from '@nestjs/common';
import {Request, Response, NextFunction} from 'express';
import {ClsService} from "nestjs-cls";
import {v4 as uuidv4} from "uuid";

@Injectable()
export class ResponseHeadersMiddleware implements NestMiddleware {
    constructor(private readonly cls: ClsService) {
    }

    use(req: Request, res: Response, next: NextFunction) {
        let reqId = req.headers['x-req-id'];
        if (!reqId) {
            reqId = uuidv4();
            req.headers['x-req-id'] = reqId;
        }
        res.setHeader('x-req-id', reqId);
        next();
    }
}