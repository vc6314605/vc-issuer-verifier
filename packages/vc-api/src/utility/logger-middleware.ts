import {Request, Response, NextFunction} from "express";
import {Injectable, NestMiddleware, Logger} from "@nestjs/common";
import {AsyncLocalStorage} from "../services/als.service.js";

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    private logger = new Logger("ACCESS_LOGS");

    constructor(private asyncLocalStorage: AsyncLocalStorage) {
    }

    use(request: Request, response: Response, next: NextFunction): void {
        var oldWrite = response.write;
        var oldEnd = response.end;
        var chunks = [];
        response.write = function (chunk: any) {
            chunks.push(chunk);
            return oldWrite.apply(response, arguments);
        };
        response.end = function (chunk: any) {
            if (chunk) {
                chunks.push(chunk);
            }
            return oldEnd.apply(response, arguments);
        };

        response.on('finish', () => {
            const responseBody = Buffer.concat(chunks).toString('utf8');
            this.logger.log(`RID:${this.asyncLocalStorage.getRequestId()} |  res-body: ${responseBody}`);
        });

        next();
    }
}