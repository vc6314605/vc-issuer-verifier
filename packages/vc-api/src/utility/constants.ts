export const contextDomain: string = `https://${process.env.ISSUER_DOMAIN_NAME}/public`;

export const vcTypes = new Map([
    ["PersonCredential", {
        context: "/context/person.jsonld",
        credentialSubjectType: "Person"
    }],
    ["EmployeeCredential", {
        context: "/context/iata.jsonld",
        credentialSubjectType: "Employee"
    }],
    ["LoyaltyMembershipCredential", {
        context: "/context/iata.jsonld",
        credentialSubjectType: "LoyaltyMembership"
    }],
    ["LoyaltyTierCredential", {
        context: "/context/iata.jsonld",
        credentialSubjectType: "LoyaltyTier"
    }],
    ["OrderCredential", {
        context: "/context/iata.jsonld",
        credentialSubjectType: "Order"
    }]
]);