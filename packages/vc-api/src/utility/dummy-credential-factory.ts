import {Injectable} from "@nestjs/common";

export enum CredentialType {
    PersonCredential, EmployeeCredential, LoyaltyMembershipCredential
}

@Injectable()
export class DummyCredentialFactory {
    private static readonly firstNames = [
        'John', 'Jane', 'Michael', 'Emily', 'David', 'Sarah', 'James', 'Olivia',
        'Robert', 'Ava', 'William', 'Sophia', 'Joseph', 'Isabella', 'Daniel', 'Mia'
    ];

    private static readonly lastNames = [
        'Smith', 'Johnson', 'Williams', 'Jones', 'Brown', 'Davis', 'Miller', 'Wilson',
        'Anderson', 'Taylor', 'Thomas', 'Jackson', 'White', 'Harris', 'Martin', 'Clark'
    ];

    private static readonly domains = [
        'gmail.com', 'yahoo.com', 'outlook.com', 'hotmail.com', 'example.com', 'test.com'
    ];

    private static readonly jobTitles = ["Software Engineer", "Product Manager", "Marketing Coordinator", "Sales Representative", "Human Resources Specialist", "Financial Analyst", "Customer Service Representative", "Graphic Designer", "Operations Manager", "Data Scientist", "Administrative Assistant", "IT Support Specialist", "Project Manager", "Accountant", "Business Development Manager", "Quality Assurance Tester"];

    private static readonly corpNames = ["TechNexa", "GlobalWave", "InnoCorp", "NovaTech", "EcoFusion", "InfiniSys", "VistaGlobal", "QuantumWave", "FuturaTech", "NexaCore", "ZenithCorp", "AquaTech", "GlobeConnect", "MetaFusion", "XenonSolutions", "SpectrumTech", "CyberNova", "SyncGlobal", "AeroDyn", "MaxiTech"]
        ;

    public static getCredential(credType: CredentialType | string | number) {
        let effectiveCredType: CredentialType;
        try {
            effectiveCredType = typeof credType === "string" ? CredentialType[credType] : credType;
        } catch (e) {
            throw Error(`Unsupported credential type : '${credType}'`, e);
        }
        switch (effectiveCredType) {
            case CredentialType.EmployeeCredential:
                return this.getRandomEmployeeCredential();
            case CredentialType.LoyaltyMembershipCredential:
                return this.getRandomLoyaltyMembershipCredential();
            case CredentialType.PersonCredential:
                return this.getRandomPersonalCredential();
            default:
                throw Error(`Unsupported credential type : '${credType}'`);
        }
    }

    private static getRandomEmployeeCredential() {
        const corp = this.getRandomCorpName();
        return {
            id: this.getRandomDid(),
            employeeId: `EMP-${+new Date}`,
            title: "Mr",
            firstName: this.getRandomFirstName(),
            lastName: this.getRandomLastName(),
            emailAddress: this.getRandomEmail(),
            jobTitle: this.getRandomJobTitle(),
            corporationName: corp,
            corporationId: corp.slice(0, 3).toUpperCase()
        };
    }

    private static getRandomPersonalCredential() {
        const corp = this.getRandomCorpName();
        return {
            id: this.getRandomDid(),
            firstName: this.getRandomFirstName(),
            lastName: this.getRandomLastName(),
            emailAddress: this.getRandomEmail(),
        };
    }

    private static getRandomLoyaltyMembershipCredential() {
        const corp = this.getRandomCorpName();
        return {
            id: this.getRandomDid(),
            membershipNumber: `EMP-${+new Date}`,
            title: "Mr",
            firstName: this.getRandomFirstName(),
            lastName: this.getRandomLastName(),
            tierLevel: this.pickRandomValue(["Basic", "Silver", "Gold", "Platinum", "Diamond"]),
            airline: this.pickRandomValue(["AA", "DL", "UA", "LH", "AF", "BA", "EK", "SQ", "QF", "CX"]),
            programName: this.pickRandomValue(["SkyMiles", "AAdvantage", "MileagePlus", "Miles & More", "Flying Blue", "Executive Club", "Skywards", "KrisFlyer", "Qantas Frequent Flyer", "Asia Miles"]
            )
        };
    }

    private static getRandomDid() {
        const jwk = {
            "alg": "ES256K",
            "use": "sig",
            "kty": "EC",
            "crv": "secp256k1",
            "x": this.getRandomString("C_4NX5li1qKWa5IebuezO5cfcs4TouonX5yRfo2l9pA".length),
            "y": "maWI5LqlhhtGFFnmVQRzJ-oU-mz6gD97fTXDDYAhDCc"
        };
        return "did:jwk:" + btoa(JSON.stringify(jwk)).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
    }

    private static getRandomString(length) {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_';
        let result = '';

        for (let i = 0; i < length; i++) {
            const randomIndex = Math.floor(Math.random() * characters.length);
            result += characters.charAt(randomIndex);
        }

        return result;
    }

    private static getRandomFirstName() {
        return this.pickRandomValue(this.firstNames);
    }

    private static getRandomLastName() {
        return this.pickRandomValue(this.lastNames);
    }

    private static getRandomDomain() {
        return this.pickRandomValue(this.domains);
    }

    private static getRandomJobTitle() {
        return this.pickRandomValue(this.jobTitles);
    }

    private static getRandomCorpName() {
        return this.pickRandomValue(this.corpNames);
    }

    private static pickRandomValue(lst: string[]) {
        return lst[Math.floor(Math.random() * lst.length)];
    }

    private static getRandomUserName() {
        return Math.random().toString(36).substring(2, 10);
    }

    private static getRandomEmail() {
        return `${this.getRandomUserName()}@${this.getRandomDomain()}`;
    }
}