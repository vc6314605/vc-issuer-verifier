export class ErrorDetails {
    constructor(public description: string, private logError: boolean) {
    }

    public exception = (msg?: string)=>{
        let error;
        if(msg){
            error = new Error(`${this.description}, ${msg}`);
        }else {
            error = new Error(this.description);
        }
        error['logError'] = this.logError;
        return error;
    }
}

const err = (description: string, logError = true): ErrorDetails => new ErrorDetails(description, logError);

export class Errors {
    public static INVALID_ACCESS_TOKEN = err("Invalid access token");
    public static INVALID_PRE_AUTH_CODE = err("Invalid pre-auth code");
    public static UNAUTHORIZED_CREDENTIAL_TYPE = err("Invalid credential type");
    public static UNKNOWN_REQUEST = err("Unknown request");
    public static INCORRECT_ISSUANCE_ID = err("Incorrect issuance Id", false);
    public static INVALID_CREDENTIAL_FORMAT = err("Invalid credential format");
    public static INVALID_STATE = err("Invalid state");
    public static INVALID_KEY_TYPE = err("Invalid state");
}

