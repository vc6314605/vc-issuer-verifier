import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {AsyncLocalStorage} from "../services/als.service.js";

@Injectable()
export class HttpInterceptor implements NestInterceptor {
  private readonly logger = new Logger(HttpInterceptor.name);

  constructor(private asyncLocalStorage: AsyncLocalStorage) {
  }

  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      map(data => {
        this.logger.debug(`RID:${this.asyncLocalStorage.getRequestId()} |  res-body: ${JSON.stringify(data)}`);
        return data;
      }),
    );
  }
}