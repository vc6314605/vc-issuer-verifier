import * as didKey from '@digitalbazaar/did-method-key';
import { securityLoader } from "@digitalbazaar/security-document-loader";
import * as vc from "@digitalbazaar/vc";
import { CachedResolver } from '@digitalcredentials/did-io';
import { Ed25519VerificationKey2020 } from '@digitalcredentials/ed25519-verification-key-2020';
import * as didWeb from '@interop/did-web-resolver';
import { CryptoLD } from 'crypto-ld';
import fetch from 'node-fetch';
import jsigs from 'jsonld-signatures';
import { Ed25519Signature2020, suiteContext } from '@digitalbazaar/ed25519-signature-2020';
import { promises as fsPromises } from 'fs';
import path from 'path';
import * as process from "process";

const { defaultDocumentLoader } = vc;

const loader = securityLoader();

const cryptoLd = new CryptoLD();
cryptoLd.use(Ed25519VerificationKey2020);

const didWebDriver = didWeb.driver({ cryptoLd });
const didKeyDriver = didKey.driver();

const resolver = new CachedResolver();
resolver.use(didWebDriver);
resolver.use(didKeyDriver);

loader.setDidResolver(resolver);

const CACHE_FOLDER = process.env.CACHED_DOC_PATH || 'cached_docs';
async function downloadPage(url) {
    try {
        // Extract the filename from the URL
        const filename = url.replace(/[^a-zA-Z0-9.-]/g, '_');

        // Check if the file is already in the cache
        const cachedFilePath = path.join(CACHE_FOLDER, filename);
        const isCached = await fileExists(cachedFilePath);

        if (isCached) {
            // If cached, read and return the cached file
            const cachedContent = await fsPromises.readFile(cachedFilePath, 'utf-8');
            console.log('Taking from cache: ', url);
            return JSON.parse(cachedContent);
        }

        console.log('Downloading: ', url);
        // If not cached, download the file
        const response = await fetch(url);
        const doc = await response.json();

        // Save the downloaded content to the cache
        await fsPromises.writeFile(cachedFilePath, JSON.stringify(doc));

        return doc;
    } catch (error) {
        console.error('URL download error', url, error);
    }
}

// Helper function to check if a file exists
async function fileExists(filePath) {
    try {
        await fsPromises.access(filePath);
        return true;
    } catch (error) {
        return false;
    }
}


export const documentLoader = jsigs.extendContextLoader(async url => {
    console.log(`documentLoader ${url}`);

    if (url.startsWith("did:web:")) {
        const domain = url.replace("did:web:", "").split("#")[0];
        const doc = await downloadPage(`https://${domain}/.well-known/did.json`)

        return {
            contextUrl: null,
            documentUrl: url,
            document: doc
        };
    }

    if (url.startsWith("https://")) {
        const doc = await downloadPage(url)
        return {
            contextUrl: null,
            documentUrl: url,
            document: doc
        };
    }

    if (url === suiteContext.CONTEXT_URL) {
        return {
            contextUrl: null,
            documentUrl: url,
            document: suiteContext.CONTEXT
        };
    }
    // did web method
    if (url.startsWith("did:web:")) {
        // fetch the context of did:web: method here
        const doc = await downloadPage(url)
        return {
            contextUrl: null,
            documentUrl: url,
            document: doc
        };
    }
    return defaultDocumentLoader(url);
});