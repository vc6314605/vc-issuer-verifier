import {Injectable, UnauthorizedException} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import {Strategy} from 'passport-http-header-strategy';
import {AuthService} from "../services/auth.service.js";

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(Strategy, 'apikey') {
    constructor(
        private readonly authService: AuthService
    ) {
        super({
            passReqToCallback: false,
            header: 'x-api-key'
        });
    }


    async validate(apiKey: string): Promise<any> {
        if (!apiKey) {
            throw new UnauthorizedException('API Key is required');
        }

        if (!this.authService.isValidApiKey(apiKey)) {
            throw new UnauthorizedException('Invalid API Key');
        }

        return {
            apiKey
        };
    }
}
