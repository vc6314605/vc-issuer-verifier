import {Body, Controller, Get, HttpCode, Param, Post, UseGuards} from '@nestjs/common';

import {
    AuthorizationRequest, AuthorizationResponse,
    CredentialIssueRequest, CredentialIssueResponse,
    GenerateCredentialOfferRequest,
    GenerateCredentialOfferResponse,
    GenerateTokenRequest,
    GenerateTokenResponse, IssuerStatus
} from "../models/dto.js";
import {IssuerService} from "../services/issuer.service.js";
import {BearerAuthToken, HostUrl} from "../utility/decorators.js";
import {AuthGuard} from "@nestjs/passport";

@Controller("api")
@UseGuards(AuthGuard())
export class ApiController {
    constructor(
        private readonly issuerService: IssuerService
    ) {
    }

    @Post('credential-offer')
    @HttpCode(200)
    async createCredentialOffer(@Body() credentialOfferRequest: GenerateCredentialOfferRequest): Promise<GenerateCredentialOfferResponse> {
        return this.issuerService.createCredentialOffer(credentialOfferRequest);
    }

    @Get('credential-offer/:state/status')
    @HttpCode(200)
    async getCredentialStatus(@Param('state') state: string): Promise<IssuerStatus> {
        return this.issuerService.getCredentialOfferStatus(state);
    }
}
