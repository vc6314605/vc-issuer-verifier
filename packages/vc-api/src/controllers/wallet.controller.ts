import {Body, Controller, Get, Header, HttpCode, HttpStatus, Param, Post, UseGuards} from '@nestjs/common';

import {
    CredentialIssueRequest, CredentialIssueResponse,
    GenerateCredentialOfferRequest,
    GenerateCredentialOfferResponse,
    GenerateTokenRequest,
    GenerateTokenResponse, IssuerStatus, PresentationVerificationRequest
} from "../models/dto.js";

import {BearerAuthToken, HostUrl} from "../utility/decorators.js";
import {AuthGuard} from "@nestjs/passport";
import {IssuerService} from "../services/issuer.service.js";
import {MetaDataService} from "../services/meta.service.js";

@Controller()
export class WalletController {
    constructor(
        private readonly issuerService: IssuerService,
        private readonly metaDataService: MetaDataService,
    ) {
    }

    @Post('token')
    @HttpCode(200)
    async generateAccessToken(@Body() tokenRequest: GenerateTokenRequest): Promise<GenerateTokenResponse> {
        return this.issuerService.generateAccessToken(tokenRequest);
    }

    @Post('credential')
    @HttpCode(200)
    async issueVC(@Body() credentialIssueRequest: CredentialIssueRequest, @BearerAuthToken() accessToken: string): Promise<CredentialIssueResponse> {
        return this.issuerService.issueVC(credentialIssueRequest, accessToken);
    }

    @Get('jwks')
    @HttpCode(200)
    async getJwks(): Promise<any> {
        return this.metaDataService.getJwks();
    }

    @Get('.well-known/openid-credential-issuer')
    @HttpCode(200)
    async getMetadata(): Promise<any> {
        return this.metaDataService.getMetadata();
    }

    @Get('.well-known/did.json')
    @HttpCode(200)
    @Header('Access-Control-Allow-Origin', '*')
    async getDid(): Promise<any> {
        return {
            "@context": [
                "https://www.w3.org/ns/did/v1",
                "https://w3id.org/security/suites/ed25519-2020/v1"
            ],
            "verificationMethod": [
                {
                    "id": `did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD`,
                    "type": "Ed25519VerificationKey2020",
                    "controller": `did:web:${process.env.ISSUER_DOMAIN_NAME}`,
                    "publicKeyMultibase": "z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"
                }
            ],
            "authentication": [
                `did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD`
            ],
            "assertionMethod": [
                `did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD`
            ],

            "id": `did:web:${process.env.ISSUER_DOMAIN_NAME}#z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD`,
            "type": "Ed25519VerificationKey2020",
            "controller": `did:web:${process.env.ISSUER_DOMAIN_NAME}`,
            "publicKeyMultibase": "z6MknTm1nFEBoEjhp7NWswFSU76eQgcT92ybQfssUB4ZpCnD"
        };
    }
}
