import {Body, Controller, Get, Param, Post, UseGuards} from '@nestjs/common';

import {
    AuthorizationRequest, AuthorizationResponse,
    CredentialIssueRequest, CredentialIssueResponse,
    GenerateCredentialOfferRequest,
    GenerateCredentialOfferResponse,
    GenerateTokenRequest,
    GenerateTokenResponse, IssuerStatus
} from "../models/dto.js";
import {IssuerService} from "../services/issuer.service.js";
import {BearerAuthToken, HostUrl} from "../utility/decorators.js";
import {AuthGuard} from "@nestjs/passport";
import {Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";
import {ApiCredential, IssuerDetails} from "../models/entity.js";

@Controller("health")
export class HealthController {
    constructor(
        @InjectRepository(ApiCredential) private readonly apiCredRepo: Repository<ApiCredential>,
        @InjectRepository(IssuerDetails) private readonly issuerRepo: Repository<IssuerDetails>
    ) {
    }


    @Get()
    async health(): Promise<any> {
        return "ok";
    }
}
