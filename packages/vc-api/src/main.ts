import {NestFactory} from '@nestjs/core';
import {ApiModule} from "./api.module.js";
import {ErrorFilter, HttpExceptionFilter} from "./utility/http-exception.filter.js";
import * as process from "process";
import morgan from 'morgan';
import path from "path";
import {NextFunction} from "express";
import * as fs from "fs";
import replaceStream from 'stream-replace';

async function bootstrap() {
    const app = await NestFactory.create(ApiModule);
    app.useGlobalFilters(new HttpExceptionFilter());
    app.useGlobalFilters(new ErrorFilter());

    app.use('/public', (req: Request, res: Response, next: NextFunction) => {
    let filePath = path.join(process.cwd(), 'public', req.url);
    if (fs.existsSync(filePath)) {
      let readStream = fs.createReadStream(filePath);
      readStream.pipe(replaceStream(/example.com/g, process.env.ISSUER_DOMAIN_NAME)).pipe(res);
    } else {
      next();
    }
  });

    morgan.token('x-forwarded-for', function (req, res) {
        return req.headers['x-forwarded-for'] || "-"
    })
    morgan.token('x-req-id', function (req, res) {
        return req.headers['x-req-id'] || "-"
    })
    morgan.token('req-body', function (req, res) {
        return JSON.stringify(req.body)
    })
    morgan.token('host', function (req, res) {
        return req.headers['host'] || "-"
    })

    app.use(morgan('ACCESS_LOGS :host RID\::x-req-id :x-forwarded-for :remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] \n\nreq-body: :req-body\n'));

    await app.listen(parseInt(process.env.APP_PORT || "3000"));
}

bootstrap();
