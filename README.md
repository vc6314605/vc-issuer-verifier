
# Description

We have implemented an issuer-as-a-service API, which uses DIDs, VCs and OID4VCI, to allow any client to delegate the process of issuing a VC, on their behalf, to the user's wallet. The clients are relieved of implementing multiple intricate procedures, and can use our simple API to issue VCs(eg. Order VC). The API is implemented using open-source libraries, and supports the current open standards for digital identity and interoperability.


![Sequence Diagram](resources/diagram.png)

# Pre-requisites

* NodeJS 18
* Install pnpm and nestjs CLI
```
RUN npm install -g pnpm@8.6.1
RUN npm install -g @nestjs/cli@10.0.0
```

* PostgresSQL(Optional, Can be run without DB as well)

# Download

```agsl
git clone https://gitlab.com/vc6314605/vc-issuer-verifier
```

# How to run
* First, Clone the repository.
* `pnpm install`
* You'll need a domain name to make this work, to test this locally, you can use serveo. This is a free tool.
* `ssh -R 80:localhost:3000 serveo.net`
* This command will provide you a domain name.
* set environment variable ISSUER_DOMAIN_NAME with domain name as below. (without https:\\\\)
* `export ISSUER_DOMAIN_NAME=68bb08232eca922abb7c89250d1953ab.serveo.net`
* Then start the app.
* `pnpm -r --stream start`

## With database
If you want to connect to an actual database, you'll need to install Postgres database and import tables and data from "resources/sql".

and provide following environment variables as well.

```
DB_HOST=<database host ip>
DB_NAME=<db name>
DB_PASS=<db password>
DB_PORT=<db port>
DB_USER=<db user>
POSTGRES_DB_ENABLED=true
ISSUER_DOMAIN_NAME=<domain name>
```

## Generate API Key
API Keys will be used for authentication and it determines the issuer details as well.
 You can create an API key and link an issuer to that in the database `api_credential` table. If you are not using a database, Some dummy data will be used from `DummyApiCredRepo` class. So the API key will be `38e29b48-0f38-4490-909a-cfb41a7f3f0c` if you are using dummy data.


## Test

### 1. Credential Offer Request

This request is used to generate a credential offer request and this will return an issuance url, You'll have to convert that to a QR using a tool like https://qr.io/. And then just scan if from your mobile wallet. Or you can use that URL as a deep link as well.

```shell
curl --location 'https://<domain-name>/api/credential-offer/' \
--header 'Content-Type: application/json' \
--header 'x-api-key: 38e29b48-0f38-4490-909a-cfb41a7f3f0c' \
--data-raw '{
    "type": "EmployeeCredential",
    "credentialSubject": {
        "numberIdentifier": 432,
        "titleName": "Mr",
        "givenName": "John",
        "surname": "Reed",
        "emailAddress": "john.r@gmail.com",
        "orgName": "MyCompany",
        "orgIdentifier": "453"
    }
}'
```

#### Sample response
```json
{
    "vcIssuanceUrl": "openid-initiate-issuance://?issuer=https%3A%2F%2F68bb08232eca922abb7c89250d1953ab.serveo.net&credential_type=EmployeeCredential&pre-authorized_code=eyJhbGciOiJFUzI1NiJ9.eyJjcmVkZW50aWFsX3R5cGUiOiJFbXBsb3llZUNyZWRlbnRpYWwiLCJ0b2tlbl90eXBlIjoiUFJFX0FVVEgiLCJpc3MiOiJodHRwczovLzY4YmIwODIzMmVjYTkyMmFiYjdjODkyNTBkMTk1M2FiLnNlcnZlby5uZXQiLCJleHAiOjE3MDc0MTk3ODZ9.HJvgN4aVlQTk8-naEdoS4DkbaPO3WTjq5MAP4fyvVfCYMBvPKYnScOExqGxM1phdQkcQWY5yCkNyWo0d0aRsbw&user_pin_required=false",
    "vcIssuanceStatusUrl": "https://68bb08232eca922abb7c89250d1953ab.serveo.net/api/credential-offer/e354979d-8f76-4425-97cd-d996176f1cb4/status"
}
```

### 2. Check Status

This URL is returned from the previous response, and it used by the client to check whether the end user has imported the VC into their wallet.

```shell
curl --location --request GET '<status-url-from-above-response>' \
--header 'x-api-key: 38e29b48-0f38-4490-909a-cfb41a7f3f0c' \
```


#### Sample response
```json
{
    "status": "In Progress"
}
```

## How to test with mobile App
- Download Sphereon wallet from https://play.google.com/store/apps/details?id=com.sphereon.ssi.wallet&hl=en&gl=US
- Make sure you are running with a domain, you can use serveo as instructed above or any other similar tool.
- Send the credential offer request.
- Then you'll get vcIssuanceUrl in the response.
- Create a QR from that URL, You can use tool like https://qr.io/.
- Scan that QR from your mobile app.


## References
- https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-08.html

