create table public.issuer (
  issuer_id integer primary key not null,
  domain character varying(200),
  alg character varying(20),
  meta_data jsonb,
  cred_type character varying(100),
  key_pair jsonb,
  did jsonb,
  token_jwk jsonb
);

create table public.api_credential (
  credential_id integer primary key not null,
  api_key character varying(100) not null,
  issuer_id integer,
  foreign key (issuer_id) references public.issuer (issuer_id)
  match simple on update no action on delete no action
);



